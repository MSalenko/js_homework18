function createClone(el) {
  const funcsObj = {
      "Object": () => {
          let cloneObj = {};
          for(let prop in el) {
            cloneObj[prop] = createClone(el[prop]);
          }
          return cloneObj;
      },
      "Array": () => {
          return el.map((i) => {
              return createClone(i);
          });
      }
  };
  if (el.constructor.name in funcsObj) {
      return funcsObj[el.constructor.name]();
  } else {
      return el;
  }
}
